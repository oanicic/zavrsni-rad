﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Zavrsni_rad
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           NavigationService.Navigate(new Uri("/popis_pretrage.xaml", UriKind.Relative));
            /*NavigationService.Navigate(new Uri("/popis_pretrage.xaml?msg=" + textBox1.Text, UriKind.Relative));*/
            
        }
       private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
       {
           NavigationService.Navigate(new Uri("/opis_odabranog.xaml", UriKind.Relative));
           
       }

    }
}